package com.example.siiaa9_2;

import java.io.Serializable;

public class Alumno implements Serializable {
    private int id;
    private String carrera;
    private String nombre;
    private Integer img;
    private String matricula;

    public Alumno(){

    }

    public Alumno(String carrera, String nombre, Integer img, String matricula){
        this.carrera = carrera;
        this.nombre = nombre;
        this.img = img;
        this.matricula;
    }

    public String getGrado(){return carrera;}
    public String getNombre(){return nombre;}
    public int getImg(){return img;}
}
